package com.rm.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rm.pojo.User;
import com.rm.service.UserService;
import com.rm.util.Layui;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    /**分页  查询*/
    /*@RequestMapping("/userPage")
    @ResponseBody
    public Layui getUserPage(Integer page,Integer limit){
    	PageHelper.startPage(page, limit);//PageHelper包 分页： page 页码 ;limit 页面数据显示条数
    	PageInfo<User> userPage = userService.getUserPage();// 返回查询数据
    	System.out.println("userPage====="+userPage);
    	//layui json数据格式
    	Layui layuiData = Layui.data((int)userPage.getTotal(),userPage.getList());

		return layuiData;
	}*/
    /**分页  查询*/
    @RequestMapping("/userPage2")
    @ResponseBody
    public Layui getUserPage2(HttpServletRequest request,HttpServletResponse response,User user){
    	int page = Integer.parseInt(request.getParameter("page"));
    	int limit = Integer.parseInt(request.getParameter("limit"));
    	
    	/**HttpSession session =request.getSession();//向Session中存取登录信息
    	if (user.getPkUser() != null) {
    		session.removeAttribute("user");//清空session信息
        	session.setAttribute("user", user);
        	//清除 session 中的所有信息:request.getSession().invalidate()
		}*/
    	
    	System.out.println("pkUser====="+user.getPkUser());
    	PageHelper.startPage(page, limit);//PageHelper包 分页： page 页码 ;limit 页面数据显示条数
    	PageInfo<User> userPage = userService.getUserPage(user);// 返回查询数据
    	System.out.println("userPage====="+userPage);
    	//layui json数据格式
    	Layui layuiData = Layui.data((int)userPage.getTotal(),userPage.getList());
    	
    	return layuiData;
    }
    
    /**删除*/
    @RequestMapping("/deleteUser")
    @ResponseBody
    public int getUserPage(User user){
		return userService.deleteUser(user.getPkUser());
	}
}

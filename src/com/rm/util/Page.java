package com.rm.util;

import java.util.List;

public class Page<T> {
	//当前页
    private int currentPage = 1;

    private int pageSize = 10;
    /** 总记录数 */
    private int total;

    private int pageCount;
    /** 开始页码 */
    private int start;

    private boolean isFirst;

    private boolean isLast;

    private List<T> list;

    private Integer[] pageCode;

    private T record;

    public T getRecord() {
        return record;
    }

    public void setRecord(T record) {
        this.record = record;
    }

    public Integer[] getPageCode() {
        if (pageCount <= pageCode.length) {
            for (int i = 0; i < pageCode.length; i++) {
                if (i >= pageCount) {
                    pageCode[i] = null;
                } else {
                    pageCode[i] = i + 1;
                }
            }
        } else {
            if (currentPage <= (pageCode.length + 1) / 2) {
                for (int i = 0; i < pageCode.length; i++) {
                    pageCode[i] = i + 1;
                }
            } else if (currentPage > pageCount - (pageCode.length + 1) / 2) {
                for (int i = 0; i < pageCode.length; i++) {
                    pageCode[i] = pageCount - pageCode.length + i + 1;
                }
            } else {
                for (int i = 0; i < pageCode.length; i++) {
                    pageCode[i] = currentPage - (pageCode.length + 1) / 2 + 1 + i;
                }
            }
        }
        return pageCode;
    }

    public Page(Integer pageCodeSize) {
        pageCode = new Integer[pageCodeSize];
    }

    public boolean getIsFirst() {
        if (currentPage == 1) {
            isFirst = true;
        }
        return isFirst;
    }

    public boolean getIsLast() {
        if (currentPage == pageCount) {
            isLast = true;
        }
        return isLast;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        if(currentPage!=null){
            this.currentPage = currentPage;
        }
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        if(pageSize!=null){
            this.pageSize = pageSize;
        }
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.pageCount = total % pageSize == 0 ? total / pageSize : total / pageSize + 1;
        this.total = total;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getStart() {
        start = (currentPage - 1) * pageSize;
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

}

package com.rm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.rm.mapper.UserMapper;
import com.rm.pojo.User;
import com.rm.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

	@Override
	public PageInfo<User> getUserPage(User user) {
		// 分页  查询数据
        List<User> userPage = userMapper.getUserPage(user);
        PageInfo<User> pageInfo = new PageInfo<User>(userPage);
		return pageInfo;
	}

	@Override
	public int deleteUser(Integer pkUser) {
		return userMapper.deleteUser(pkUser);
	}

   
}

package com.rm.service;

import com.github.pagehelper.PageInfo;
import com.rm.pojo.User;

public interface UserService {

  	/**分页 查询全部*/
	PageInfo<User> getUserPage(User user);
	//PageInfo<User> getUserPage(Integer pkUser);
	
	int deleteUser(Integer pkUser);
}
